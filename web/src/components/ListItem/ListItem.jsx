import React from 'react';
import PropTypes from 'prop-types';

import styles from './ListItem.module.scss'

const ListItem = ({ data, onClickItem }) => {
    const releaseYear = new Date(data.release_date).getFullYear().toString();
    return (
        <div className={styles.listItem} onClick={onClickItem}>
            <img className={styles.listItemImg} src={data.poster_path} alt={data.title}/>
            <h3 className={styles.listItemTitle}>{data.title}</h3>
            <p className={styles.listItem}>{releaseYear}</p>
        </div>
    );
};

ListItem.defaultProps = {
    data: {},
};

ListItem.propTypes = {
    data: PropTypes.object
};

export default ListItem;
