import React from 'react';
import { render } from '../../tests/helper'

import ListItem from '../../components/ListItem';
describe('ListItem Tests', () => {


  const setup = (props) => {
    return render(<ListItem {...props}/>);
  };
  it('should render ListItem', async () => {
    const { asFragment } = setup({
      data: {}
    });

    expect(asFragment()).toMatchSnapshot();
  });

});
