import React from "react";
import PropTypes from "prop-types";
import backArrow from '../../assets/back-arrow.svg';
import forwardArrow from '../../assets/forward-arrow.svg';

import styles from "./Pagination.module.scss";

const Pagination = ({ page = 0, total_results = 0, total_pages = 0, handleOnClickNext, handleOnClickPrev }) => {
    return (<div className={styles.pagination} data-testid="pagination">
        <div className={styles.paginationResults}>
            {total_results} Results found
        </div>
        <div className={styles.pager}>
            <span>Page {page} of {total_pages}</span>
            <div className={styles.pagerBtns}>
                <button className={styles.pagerBtn} onClick={handleOnClickPrev}>
                    <img src={backArrow} alt="back arrow"/>
                </button>
                <button className={styles.pagerBtn} onClick={handleOnClickNext}>
                    <img src={forwardArrow} alt="forward arrow"/>
                </button>
            </div>
        </div>
    </div>);
};

Pagination.propTypes = {
    children: PropTypes.any,
};

export default Pagination;
