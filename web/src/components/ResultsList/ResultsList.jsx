import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types'
import ListItem from "../ListItem";
import List from "../List";
import Pagination from "../../containers/PaginationContainer";
import LoadingSpinner from "../LoadingSpinner";
import Modal from "../Modal";
import MovieDetail from "../MovieDetail/MovieDetail";
import styles from './ResultsList.module.scss';
const ResultsList = ({ results, selected, handleGetMovieById, isMoviesSearchLoading, isMovieLoading}) => {
    const [showModal, setShowModal] = useState(false);
    const [selectedItem, setSelectedItem] = useState({});
    useEffect(() => {
        if(Object.keys(selected).length > 0 && !isMovieLoading) {
            setSelectedItem(selected)
        }
    }, [selected, isMovieLoading]);
    const handleToggleModal = () => {
        setShowModal(!showModal);
        setSelectedItem({})
    };
    const handleClickItem = (item) => {
        setShowModal(!showModal);
        handleGetMovieById(item.id)
    };
    if(isMoviesSearchLoading) {
        return <div className={styles.resultsListLoading}>
            <LoadingSpinner isLoading={isMoviesSearchLoading}/>
        </div>
    }
    if(results.length < 1) {
        return 'No results'
    }
    return (
        <div className={styles.resultsList} data-testid="results-list">

            <Pagination/>
            <List>
                {results && results.map(item => <ListItem onClickItem={() => handleClickItem(item)} key={item.id} data={item}/>)}
            </List>
            <Modal show={showModal} handleClose={handleToggleModal}>
                {isMovieLoading ? <LoadingSpinner isLoading/> : <MovieDetail data={selectedItem}/>}
            </Modal>
        </div>
    );
};

ResultsList.defaultProps = {
    results: [],
    selected: {},
    isMoviesSearchLoading: false,
    isMovieLoading: false,
    handleOnClickItem: () => {},
    handleGetMovieById: () => {}
};

ResultsList.propTypes = {
    data: PropTypes.array,
    handleOnClickItem: PropTypes.func,
};

export default React.memo(ResultsList);
