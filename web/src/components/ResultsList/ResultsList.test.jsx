import React from 'react';
import { render, screen, fireEvent, cleanup, waitForElement } from '../../tests/helper'
import ResultsList from './ResultsList';
import {mockConfigResponse, mockSearchResponse} from "../../tests/__mocks__/data";
import {mapResultsData} from "../../state/ducks/selectors";

describe('App Tests', () => {
    afterEach(cleanup);
    const resultsResponse = mapResultsData({
        config: mockConfigResponse,
        movies: {
            data: mockSearchResponse
        }
    });
    const setup = (props) => {
        return render(<ResultsList {...props}/>);
    };

    it('should render results list with no results', () => {
        const { asFragment, getByText } = setup();
        expect(asFragment()).toMatchSnapshot();
        expect(getByText('No results')).toBeInTheDocument();
    });

    it('should render loading spinner', () => {
        const { queryByTestId } = setup({
            isMoviesSearchLoading: true
        });
        expect(queryByTestId('loading-spinner')).toBeInTheDocument();
    });

    it('should render results list with results', () => {
        const { getByText } = setup({
            results: resultsResponse
        });
        expect(getByText('The Avengers')).toBeInTheDocument();
    });

    it('should call handleGetMovieById when item is clicked', () => {
        const handleGetMovieByIdMock = jest.fn();
        const { queryByTestId } = setup({
            handleGetMovieById: handleGetMovieByIdMock,
            results: resultsResponse
        });
        fireEvent.click(queryByTestId('list-container').children[0]);
        expect(handleGetMovieByIdMock).toHaveBeenCalledWith(1);
    });
});
