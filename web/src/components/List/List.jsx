import React from 'react';
import PropTypes from 'prop-types'
import styles from './List.module.scss'

const List = ({ children }) => {
    return (
        <div className={styles.listContainer} data-testid="list-container">
            {children}
        </div>
    );
};

List.propTypes = {
    children: PropTypes.any,
};

export default React.memo(List);
