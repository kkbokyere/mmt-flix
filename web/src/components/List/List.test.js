import React from 'react';
import { render } from '../../tests/helper'

import List from '../../components/List';
describe('List Tests', () => {

  const setup = (props) => {
    return render(<List {...props}/>);
  };
  it('should render List', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

});
