import React from "react";
import PropTypes from 'prop-types';
import styles from './MovieDetail.module.scss';
const MovieDetail = ({ data }) => {
    const { poster_path, title, release_date, overview, director, cast, genres} = data;
    return(
        <div className={styles.movieDetail}>
            <img className={styles.movieDetailImg} src={poster_path} alt={title}/>

            <div>
                <h2 className={styles.movieDetailTitle}>
                    {title}
                    <span className={styles.movieDetailYear}> ({new Date(release_date).getFullYear().toString()})</span>
                </h2>

                <p>Director: <b>{director}</b></p>
                <p>Cast: <b>{cast}</b></p>
                <p>Genre: <b>{genres}</b></p>
                <p>{overview}</p>
            </div>
        </div>
    )
};

MovieDetail.propTypes = {
    data: PropTypes.object
};

export default React.memo(MovieDetail);
