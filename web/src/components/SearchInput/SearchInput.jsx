import React, {useState} from 'react';
import PropTypes from 'prop-types'
import styles from './SearchInput.module.scss'
import searchIcon from '../../assets/search.svg'
const SearchInput = ({ handleSearch }) => {
    const [searchTerm, setSearchTerm] = useState('');
    const handleOnSubmit = (e) => {
        handleSearch(searchTerm);
        e.preventDefault()
    };

    const handleOnChange = ({ currentTarget: {value}}) => {
        setSearchTerm(value);
    };
    return (
        <form onSubmit={handleOnSubmit} data-testid="search-form" className={styles.searchForm}>
            <input data-testid="search-input" type='text' placeholder="Search here..." onChange={handleOnChange} className={styles.searchInput}/>
            <button type="submit" className={styles.searchInputBtn}><img src={searchIcon} alt='search icon'/></button>
        </form>
    );
};

SearchInput.propTypes = {
    children: PropTypes.any,
};

export default SearchInput;
