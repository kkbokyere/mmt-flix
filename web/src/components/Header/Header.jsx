import React from "react";
import logo from '../../assets/logo.svg';
import SearchContainer from '../../containers/SearchContainer'
import PropTypes from "prop-types";

import styles from "./Header.module.scss";

const Header = ({ children }) => {
  return (<header className={styles.header} >
    <img src={logo} className="App-logo" alt="logo" />
    <SearchContainer/>
  </header>);
};

Header.propTypes = {
  children: PropTypes.any,
};

export default Header;
