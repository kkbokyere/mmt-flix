import React from "react";
import { render } from "../../tests/helper";

import Header from "../../components/Header";
describe("Header Tests", () => {
  const setup = (props) => {
    return render(<Header {...props} />);
  };
  it("should render Header", async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });
});
