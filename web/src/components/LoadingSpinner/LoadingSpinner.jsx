import React from 'react';
import styles from './LoadingSpinner.module.scss'
import PropTypes from 'prop-types';

const LoadingSpinner = ({ isLoading }) => {
    if(!isLoading) {
        return null
    }
    return (<div data-testid="loading-spinner" className={styles.loader}>Loading...</div>)
};
LoadingSpinner.propTypes = {
    isLoading: PropTypes.bool
};

export default LoadingSpinner;
