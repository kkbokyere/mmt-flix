import React from 'react';
import { render, screen, fireEvent, cleanup, waitForElement } from './tests/helper'
import App from './App';

describe('App Tests', () => {
  afterEach(cleanup);

  const setup = (props) => {
    return render(<App/>, props);
  };

  describe('Setup', () => {
    it('should render app', () => {
      const { asFragment } = setup();
      expect(asFragment()).toMatchSnapshot();
    });

    it('should render initial elements', () => {
      const { queryByTestId, getByText } = setup();
      expect(queryByTestId('search-input')).toBeInTheDocument();
      expect(getByText('No results')).toBeInTheDocument();
      expect(queryByTestId('pagination')).not.toBeInTheDocument();
    });

  });

  describe('Search', () => {

    const runSearch = async () => {
      const { queryByTestId } = setup();

      fireEvent.change(queryByTestId('search-input'), { target: { value: "some title"}});
      fireEvent.submit(queryByTestId('search-form'));
      const resultsList = await waitForElement(() => queryByTestId('results-list'));
      return {
        resultsList
      }
    };
    it('should search for a movie', async () => {
      const { resultsList } = await runSearch();
      expect(resultsList).toBeInTheDocument();
      expect(screen.queryByTestId('pagination')).toBeInTheDocument();
      expect(screen.queryByTestId('list-container').children.length).toBe(1);
      expect(screen.getByText('The Avengers')).toBeInTheDocument()
    });

    it('open modal and see further information about movie', async () => {

      expect(screen.queryByTestId('modal')).not.toBeInTheDocument();
      await runSearch();
      fireEvent.click(screen.queryByTestId('list-container').children[0]);
      expect(screen.queryByTestId('modal')).toBeInTheDocument();
      await waitForElement(() => screen.getByText('Tony Stark / Iron Man'));
      expect(screen.getByText('Tony Stark / Iron Man')).toBeInTheDocument();
      expect(screen.getByText('Alan Silvestri')).toBeInTheDocument();
      expect(screen.getByText('Avengers: Endgame')).toBeInTheDocument();
      expect(screen.getByText('(2019)')).toBeInTheDocument()
    })

  })
});
