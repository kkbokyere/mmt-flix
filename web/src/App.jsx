import React from 'react';
import "./styles/App.scss";
import Header from "./components/Header";
import Layout from "./components/Layout";
import ResultsListContainer from "./containers/ResultsListContainer";

function App() {
  return (
    <div className="App">
      <Header/>
      <Layout>
          <ResultsListContainer/>
      </Layout>
    </div>
  );
}

export default App;
