
import {mockSearchResponse, mockConfigResponse, mockMovieResponse, mockCreditsResponse} from "./data";
const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;

export default {
  spread: jest.fn(),
  all: jest.fn(() => {
    return Promise.resolve({movie: mockMovieResponse, credits: mockCreditsResponse});
  }),
  get: jest.fn((url) => {
    switch (url) {
      case `${API_URL}/configuration?api_key=${API_KEY}`:
        return Promise.resolve({data: mockConfigResponse});
      case `${API_URL}/search/movie?api_key=${API_KEY}`:
        return Promise.resolve({ data: mockSearchResponse});
      case `${API_URL}/movie/1?api_key=${API_KEY}`:
        return Promise.resolve({data: mockMovieResponse});
      case `${API_URL}/movie/1/credits?api_key=${API_KEY}`:
        return Promise.resolve({ data: mockCreditsResponse});
      default:
        return Promise.resolve(mockSearchResponse)

    }
  }),
};
