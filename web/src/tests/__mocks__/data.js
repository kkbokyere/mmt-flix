export const mockSearchResponse = {
    "page": 1,
    "total_results": 14,
    "total_pages": 1,
    results: [{
        "poster_path": "/cezWGskPY5x7GaglTTRN4Fugfb8.jpg",
        "adult": false,
        "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
        "release_date": "2012-04-25",
        "genre_ids": [
            878,
            28,
            12
        ],
        "id": 1,
        "original_title": "The Avengers",
        "original_language": "en",
        "title": "The Avengers",
        "backdrop_path": "/hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg",
        "popularity": 7.353212,
        "vote_count": 8503,
        "video": false,
        "vote_average": 7.33
    }]
};
export const mockConfigResponse = {
    "images": {
        "base_url": "http://image.tmdb.org/t/p/",
        "poster_sizes": [
            "w92",
            "w154",
            "w185",
            "w342",
            "w500",
            "w780",
            "original"
        ]
    }
};
export const mockCreditsResponse = {
    "id": "21",
    cast: [{
        cast_id: 493,
        character: "Tony Stark / Iron Man",
        credit_id: "5e85cd735294e700134abf26",
        gender: 2,
        id: 3223,
        name: "Robert Downey Jr.",
        order: 0,
        profile_path: "/e6I9Q4GanZaljGjcpEi9fFqAhtF.jpg",
    }],
    crew: [{
        credit_id: "57564fe09251416e3600172a",
        department: "Director",
        gender: 2,
        id: 37,
        job: "Director",
        name: "Alan Silvestri",
        profile_path: "/pQOAsQDuYMR4cKaPAP0wkRlCSNo.jpg",
    }]
};
export const mockMovieResponse = {
    "adult": false,
    "backdrop_path": "/orjiB3oUIsyz60hoEqkiGpy5CeO.jpg",
    "belongs_to_collection": {
        "id": 86311,
        "name": "The Avengers Collection",
        "poster_path": "/tqXiOD5rTyHgabO73Tpw9JDbd88.jpg",
        "backdrop_path": "/zuW6fOiusv4X9nnW3paHGfXcSll.jpg"
    },
    "budget": 356000000,
    "genres": [{"id": 12, "name": "Adventure"}, {"id": 878, "name": "Science Fiction"}, {"id": 28, "name": "Action"}],
    "homepage": "https://www.marvel.com/movies/avengers-endgame",
    "id": 299534,
    "imdb_id": "tt4154796",
    "original_language": "en",
    "original_title": "Avengers: Endgame",
    "overview": "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
    "popularity": 218.797,
    "poster_path": "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
    "production_companies": [{
        "id": 420,
        "logo_path": "/hUzeosd33nzE5MCNsZxCGEKTXaQ.png",
        "name": "Marvel Studios",
        "origin_country": "US"
    }],
    "production_countries": [{"iso_3166_1": "US", "name": "United States of America"}],
    "release_date": "2019-04-24",
    "revenue": 2797800564,
    "runtime": 181,
    "spoken_languages": [{"iso_639_1": "en", "name": "English"}, {"iso_639_1": "ja", "name": "日本語"}, {
        "iso_639_1": "xh",
        "name": ""
    }],
    "status": "Released",
    "tagline": "Part of the journey is the end.",
    "title": "Avengers: Endgame",
    "video": false,
    "vote_average": 8.3,
    "vote_count": 15289
};
