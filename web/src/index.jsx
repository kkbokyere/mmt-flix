import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";

import "./styles/index.scss";

import App from './App';
import * as serviceWorker from './serviceWorker';

import configureStore from "./state/store";
import {fetchConfig} from "./state/ducks/configuration";

const store = configureStore();

store.dispatch(fetchConfig());
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
