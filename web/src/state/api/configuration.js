import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;
export const API_KEY = process.env.REACT_APP_API_KEY;

export const configurationApi = (params) => {
  return axios.get(`${API_URL}/configuration?api_key=${API_KEY}`, { params }).then((response) => {
    return response.data;
  });
};
