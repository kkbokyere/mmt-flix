import axios from "axios";
export const API_URL = process.env.REACT_APP_API_URL;
export const API_KEY = process.env.REACT_APP_API_KEY;

export const searchMovies = (params) => {
  return axios.get(`${API_URL}/search/movie?api_key=${API_KEY}`, { params }).then((response) => {
    return response.data;
  });
};

export const getMovieById = (movie_id) => {
  const movieRequest = axios.get(`${API_URL}/movie/${movie_id}?api_key=${API_KEY}`);
  const creditsRequest = axios.get(`${API_URL}/movie/${movie_id}/credits?api_key=${API_KEY}`);
  return axios.all([movieRequest, creditsRequest]).then(axios.spread((...responses) => {
    const responseOne = responses[0].data;
    const responseTwo = responses[1].data;
    return { movie:responseOne, credits: responseTwo}
  }))
};
