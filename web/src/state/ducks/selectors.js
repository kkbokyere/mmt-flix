import { createSelector } from 'reselect'

const getResults = (state) => state.movies.data.results;
const getSelected = (state) => state.movies.selected;
const getConfig = (state) => state.config;

export const mapResultsData = createSelector(
    [getResults, getConfig],
    (results, config) => {
        return results.map((item) => ({
            ...item,
            poster_path: `${config.images.base_url}${config.images.poster_sizes[3]}${item.poster_path}`
        }))
    });

export const mapSelectedData = createSelector(
    [getSelected, getConfig],
    (selected, config) => {
        if(Object.keys(selected).length < 1) {
            return {}
        }
        const { movie, credits } = selected;
        return {
            ...selected,
            title: movie.title,
            overview: movie.overview,
            director: credits.crew.find(({ job }) => job === "Director").name,
            cast: credits.cast.map(({ character }) => character).join(", "),
            genres: movie.genres.map(({ name }) => name).join(", "),
            release_date: movie.release_date,
            poster_path: `${config.images.base_url}${config.images.poster_sizes[3]}${movie.poster_path}`
        }
    });
