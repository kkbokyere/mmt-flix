import {getMovieById, searchMovies} from "../api/movies";

export const SEARCH_MOVIES_REQUEST = "SEARCH_MOVIES_REQUEST";
export const SEARCH_MOVIES_SUCCESS = "SEARCH_MOVIES_SUCCESS";
export const SEARCH_MOVIES_FAILURE = "SEARCH_MOVIES_FAILURE";

export const GET_MOVIE_BY_ID_REQUEST = "GET_MOVIE_BY_ID_REQUEST";
export const GET_MOVIE_BY_ID_SUCCESS = "GET_MOVIE_BY_ID_SUCCESS";
export const GET_MOVIE_BY_ID_FAILURE = "GET_MOVIE_BY_ID_FAILURE";

export const SET_SEARCH_PARAMS = "SET_SEARCH_PARAMS";

export const initialState = {
  isMoviesSearchLoading: false,
  isMovieLoading: false,
  error: null,
  selected: {},
  params: {
    query: '',
    page: 1,
  },
  data: {
    results: []
  },
};

function searchMoviesRequest(payload) {
  return {
    type: SEARCH_MOVIES_REQUEST,
    payload,
  };
}

function searchMoviesSuccess(payload) {
  return {
    type: SEARCH_MOVIES_SUCCESS,
    payload,
  };
}

function searchMoviesFailure(payload) {
  return {
    type: SEARCH_MOVIES_FAILURE,
    payload,
  };
}

function getMovieByIdRequest(payload) {
  return {
    type: GET_MOVIE_BY_ID_REQUEST,
    payload,
  };
}

function getMovieByIdSuccess(payload) {
  return {
    type: GET_MOVIE_BY_ID_SUCCESS,
    payload,
  };
}

function getMovieByIdFailure(payload) {
  return {
    type: GET_MOVIE_BY_ID_FAILURE,
    payload,
  };
}

function setSearchParams(payload) {
  return {
    type: SET_SEARCH_PARAMS,
    payload,
  };
}

export function fetchNextPage() {
  return function (dispatch, getState) {
    const currentParams = getState().movies.params;
    const totalPages = getState().movies.data.total_pages;
    const nextPage = currentParams.page + 1;
    if(nextPage <= totalPages) {
      dispatch(fetchMovies({ ...currentParams, page: nextPage}))
    }
  }
}
export function fetchPrevPage() {
  return function (dispatch, getState) {
    const currentParams = getState().movies.params;
    const prevPage = currentParams.page - 1;
    if(prevPage > 0) {
      dispatch(fetchMovies({ ...currentParams, page: prevPage}))
    }
  }
}

export function fetchMovies(params) {
  return function (dispatch) {
    dispatch(searchMoviesRequest(params));
    return searchMovies(params)
      .then((response) => {
        dispatch(searchMoviesSuccess(response));
        dispatch(setSearchParams(params));
      })
      .catch((error) => {
        dispatch(searchMoviesFailure(error.status_message));
      });
  };
}

export function fetchMovieById(id) {
  return function (dispatch) {
    dispatch(getMovieByIdRequest(id));
    return getMovieById(id)
        .then((response) => {
          dispatch(getMovieByIdSuccess(response));
        })
        .catch((error) => {
          dispatch(getMovieByIdFailure(error.status_message));
        });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case SEARCH_MOVIES_SUCCESS:
      return {
        ...state,
        data: payload,
        isMoviesSearchLoading: false,
      };
    case SEARCH_MOVIES_FAILURE:
      return {
        ...state,
        isMoviesSearchLoading: false,
        error: payload,
      };
    case SEARCH_MOVIES_REQUEST:
      return {
        ...state,
        error: null,
        isMoviesSearchLoading: true,
      };
    case GET_MOVIE_BY_ID_SUCCESS:
      return {
        ...state,
        selected: payload,
        isMovieLoading: false,
      };
    case GET_MOVIE_BY_ID_FAILURE:
      return {
        ...state,
        isMovieLoading: false,
        error: payload,
      };
    case GET_MOVIE_BY_ID_REQUEST:
      return {
        ...state,
        error: null,
        selected: {},
        isMovieLoading: true,
      };
    case SET_SEARCH_PARAMS:
      return {
        ...state,
        params: {...state.params, ...payload}
      };
    default:
      return state;
  }
};
