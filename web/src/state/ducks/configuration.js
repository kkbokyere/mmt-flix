import { configurationApi } from "../api/configuration";

export const GET_CONFIGURATION_REQUEST = "GET_CONFIGURATION_REQUEST";
export const GET_CONFIGURATION_SUCCESS = "GET_CONFIGURATION_SUCCESS";
export const GET_CONFIGURATION_FAILURE = "GET_CONFIGURATION_FAILURE";

export const initialState = {
  isLoading: false,
  error: null
};

function getConfigRequest(payload) {
  return {
    type: GET_CONFIGURATION_REQUEST,
    payload,
  };
}

function getConfigSuccess(payload) {
  return {
    type: GET_CONFIGURATION_SUCCESS,
    payload,
  };
}

function getConfigFailure(payload) {
  return {
    type: GET_CONFIGURATION_FAILURE,
    payload,
  };
}

export function fetchConfig() {
  return function (dispatch) {
    dispatch(getConfigRequest());
    return configurationApi()
      .then((response) => {
        dispatch(getConfigSuccess(response));
      })
      .catch((error) => {
        dispatch(getConfigFailure(error.status_message));
      });
  };
}

export default (state = initialState, action) => {
  let { payload } = action;
  switch (action.type) {
    case GET_CONFIGURATION_SUCCESS:
      return {
        ...state,
        ...payload,
        isLoading: false,
      };
    case GET_CONFIGURATION_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    case GET_CONFIGURATION_REQUEST:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    default:
      return state;
  }
};
