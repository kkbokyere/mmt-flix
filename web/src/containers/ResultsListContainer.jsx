import { connect } from 'react-redux'
import ResultsList from "../components/ResultsList";
import {mapResultsData, mapSelectedData} from "../state/ducks/selectors";
import {fetchMovieById} from "../state/ducks/movies";
const mapStateToProps = (state, ownProps) => {
    return {
        isMoviesSearchLoading: state.movies.isMoviesSearchLoading,
        isMovieLoading: state.movies.isMovieLoading,
        selected: mapSelectedData(state),
        results: mapResultsData(state),
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch ) => {
    return {
        handleGetMovieById: (movie_id) => dispatch(fetchMovieById(movie_id))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResultsList)
