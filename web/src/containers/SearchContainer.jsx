import { connect } from 'react-redux'
import SearchInput from "../components/SearchInput";
import {fetchMovies} from "../state/ducks/movies";
const mapStateToProps = ({ movies }, ownProps) => {
    return {
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch ) => {
    return {
        handleSearch: (query) => dispatch(fetchMovies({ query, page: 1 }))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchInput)
