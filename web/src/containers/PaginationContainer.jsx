import { connect } from 'react-redux'
import {fetchNextPage, fetchPrevPage} from "../state/ducks/movies";
import Pagination from "../components/Pagination";
const mapStateToProps = ({ movies }, ownProps) => {
    return {
        ...movies.data,
        ...ownProps
    }
};

const mapDispatchToProps = (dispatch ) => {
    return {
        handleOnClickNext: () => dispatch(fetchNextPage()),
        handleOnClickPrev: () => dispatch(fetchPrevPage())
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Pagination)
